<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Patient extends Model
{
    public function physiotherapist() {
        return $this->belongsTo('App\Physiotherapist');
    }

    public function results() {
        return $this->hasMany('App\Result')->orderBy('date', 'DESC');
    }
}
