<?php
/**
 * Created by PhpStorm.
 * User: IvoLaptop
 * Date: 11-May-18
 * Time: 21:43
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Physiotherapist;
use Illuminate\Support\Facades\Auth;
use Validator;

class LoginController extends Controller
{
    public $successStatus = 200;

    /**
     * Register api
     *
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required',
            'c_password' => 'required|same:password',
            'firstname' => 'required',
            'lastname' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 401);
        }
        $input = $request->all();
        $input['password'] = bcrypt($input['password']);
        $physiotherapist = Physiotherapist::create($input);
        return response()->json(['message' => 'User successfully registered!'], $this-> successStatus);
    }
}