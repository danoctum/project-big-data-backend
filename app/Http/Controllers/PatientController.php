<?php

namespace App\Http\Controllers;

use App\Result;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Patient;
use App\Http\Resources\Patient as PatientResource;
use App\Http\Resources\PatientWithResults as PatientWithResultsResource;
use App\Http\Resources\Result as ResultResource;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Auth;
use Faker\Provider\Base as Faker;

class PatientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $patients = Patient::where('physiotherapist_id', auth::id())->get();
        return PatientResource::collection($patients);
    }

    public function getPatientApp($token) {
        $patient = Patient::where('token', $token)->get();



        if($patient->isEmpty()) {
            return response()->json(['status' => false]);
        } else {
            return response()->json(['status' => true,
                'id' => $patient[0]->id,
                'firstname' => $patient[0]->firstname,
                'lastname' => $patient[0]->lastname,
                'avatar' => $patient[0]->avatar,
                'sex' => $patient[0]->sex,
                'birthdate' => date("d-m-Y", strtotime($patient[0]->birthday)),
                'telnr' => $patient[0]->telnr,
                'physiotherapist_id' => $patient[0]->physiotherapist_id,
                'token' => $patient[0]->token,
                'max_bpm' => $patient[0]->max_bpm,
                'goal' => $patient[0]->goal,
                'weight'=> $patient[0]->weight,
                'length' => $patient[0]->length,
                'emailFamily' => $patient[0]->emailFamily]); //Moet collection zijn omdat get() een coillection terug geeft
        }
    }

    public function setPatientSteps($token, $steps) {
        $patient = Patient::where('token', $token)->get();
        if($patient->isEmpty()) {
            return response()->json('status', false);
        }
        $result = Result::where(['date' => date('Y-m-d'), 'patient_id' => $patient[0]->id])->get();
        if($result->isEmpty()) {
            $result = new Result();
            $result->patient_id = $patient[0]->id;
            $result->date = date('Y-m-d');
            $result->result = (int)$steps;
            $result->goal = $patient[0]->goal;
            $result->save();
            return response()->json([
                'patient_id' => $result[0]->patient_id,
                'goal' => $result[0]->goal,
                'date' => date("d-m-Y", strtotime($result[0]->date)),
                'result' => $result[0]->result,
                'created_at' => $result[0]->created_at,
                'updated_at' => $result[0]->updated_at]);
        } else {
            $result[0]->result = (int)$steps;
            $result[0]->save();
            return response()->json([
                'patient_id' => $result[0]->patient_id,
                'goal' => $result[0]->goal,
                'date' => date("d-m-Y", strtotime($result[0]->date)),
                'result' => $result[0]->result,
                'created_at' => $result[0]->created_at,
                'updated_at' => $result[0]->updated_at]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) //UPDATE AND SAVE PATIENT!
    {
        if(isset($request->id)) {
            $patient = Patient::findOrFail($request->id);
        } else {
            $patient = new Patient;
            $patient->token = Faker::asciify("**********");
            while(empty(Patient::where('token', $patient->token)->get())) {
                $patient->token = Faker::asciify("**********");
            }
        }
        $patient->firstname = $request->input('firstname');
        $patient->lastname = $request->input('lastname');
        $patient->birthday = date("Y-m-d", strtotime($request->input('birthdate')));
        $patient->sex = $request->input('sex');
        $patient->emailFamily = $request->input('emailFamily');
        $patient->length = $request->input('length');
        $patient->telnr = $request->input('telnr');
        $patient->weight = $request->input('weight');
        $patient->goal = $request->input('goal');
        $patient->max_bpm = $request->input('max_bpm');
        $patient->physiotherapist_id = auth::id();
        if($patient->save()) {
            return new PatientResource($patient);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $patient =  Patient::findOrFail($id);
        return new PatientWithResultsResource($patient);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
