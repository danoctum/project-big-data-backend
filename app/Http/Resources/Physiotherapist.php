<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Physiotherapist extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        //return parent::toArray($request);
        return [
            'id' => $this->id,
            'firstname' => $this->firstname,
            'lastname' => $this->lastname,
            'avatar' => $this->avatar,
            'email' => $this->email,
            'patients' => $this->patients
        ];
    }
}
