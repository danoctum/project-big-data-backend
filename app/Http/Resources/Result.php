<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Result extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'patient_id' => $this->patient_id,
            'goal' => $this->goal,
            'date' => date("d-m-Y", strtotime($this->date)),
            'result' => $this->result,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at
        ];
    }
}
