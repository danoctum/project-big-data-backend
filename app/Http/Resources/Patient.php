<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Patient extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'firstname' => $this->firstname,
            'lastname' => $this->lastname,
            'avatar' => $this->avatar,
            'sex' => $this->sex,
            'birthdate' => date("d-m-Y", strtotime($this->birthday)),
            'telnr' => $this->telnr,
            'physiotherapist_id' => $this->physiotherapist_id,
            'token' => $this->token,
            'max_bpm' => $this->max_bpm,
            'goal' => $this->goal,
            'weight'=> $this->weight,
            'length' => $this->length,
            'emailFamily' => $this->emailFamily,
        ];
    }
}
