<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Group van JWT
Route::group([
    'prefix' => 'auth'
], function ($router) {
    Route::post('login', 'AuthController@login');
    Route::post('logout', 'AuthController@logout');
    Route::post('refresh', 'AuthController@refresh');
    Route::post('me', 'AuthController@me');
});

Route::group([
    'middleware' => 'jwt.auth'
], function ($router) {

    Route::get('therapists', 'PhysiotherapistController@index');

    Route::get('therapists/{id}', 'PhysiotherapistController@show');

    Route::post('therapists', 'PhysiotherapistController@store');

    Route::put('therapists', 'PhysiotherapistController@store');

    Route::delete('therapists', 'PhysiotherapistController@destroy');

    Route::get('patients/{id}', 'PatientController@show');

    Route::get('patients', 'PatientController@index');

    Route::post('patients', 'PatientController@store');

    Route::patch('patients/{id}', 'PatientController@store');

    Route::POST('userRegister', 'LoginController@register');
});

//Routes voor App

Route::get('patientApp/{token}', 'PatientController@getPatientApp');

Route::get('patientSteps/{token}/{steps}', 'PatientController@setPatientSteps');


