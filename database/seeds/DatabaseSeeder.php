<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Physiotherapist::class, 4)->create();
        factory(App\Patient::class, 40)->create();
        factory(App\Result::class, 2000)->create();
    }
}
