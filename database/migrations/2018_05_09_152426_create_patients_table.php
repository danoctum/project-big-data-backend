<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePatientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('patients', function (Blueprint $table) {
            $table->increments('id');
            $table->string('firstname');
            $table->string('lastname');
            $table->string('avatar')->nullable();
            $table->integer('sex');
            $table->date('birthday');
            $table->string('telnr')->nullable();
            $table->integer('length')->nullable();
            $table->float('weight')->nullable();
            $table->string('token');
            $table->string('emailFamily')->nullable();
            $table->unsignedInteger('physiotherapist_id');
            $table->foreign('physiotherapist_id')->references('id')->on('physiotherapists');
            $table->integer('max_bpm')->nullable();
            $table->integer('goal')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('patient');
    }
}
