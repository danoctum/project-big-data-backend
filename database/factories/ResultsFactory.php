<?php

use Faker\Generator as Faker;

$factory->define(App\Result::class, function (Faker $faker) {
    $date = date('Y-m-d');
    return [
        'patient_id' => random_int(\DB::table('patients')->min('id'), \DB::table('patients')->max('id')),
        'date' => $faker->dateTimeBetween($startDate = '-21 days', $endDate = 'now')->format('Y-m-d'),
        'result' => $faker->numberBetween(500,12000),
        'goal' => $faker->numberBetween(2000,14000),
    ];
});
