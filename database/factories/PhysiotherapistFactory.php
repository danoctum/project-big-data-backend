<?php

use Faker\Generator as Faker;

$factory->define(App\Physiotherapist::class, function (Faker $faker) {
    return [
        'firstname' => $faker->firstName(),
        'lastname' => $faker->lastName(),
        'avatar' => array("https://www.fysiovandaag.nl/userfiles/image/Foto_frank_klein.jpg", null)[array_rand(array(1,0), 1)],
        'email' => $faker->email(),
        'password' => bcrypt('12345')
    ];
});
