<?php

use Faker\Generator as Faker;

$factory->define(App\Patient::class, function (Faker $faker) {
    return [
        'firstname' => $faker->firstName(),
        'lastname' => $faker->lastName(),
        'avatar' => array("https://p.fod4.com/p/channels/refvx/profile/s=w1200/o=95/5AjLRRSmREm1CXDYsAhk_danny_trejo.jpg", "https://thumbs.werkaandemuur.nl/6a962694ebbfb444e709f56d862ba59f_950x600_fit.jpg",null)[array_rand(array(1,0,3), 1)],
        'sex' => $faker->numberBetween(0,1),
        'length' => $faker->numberBetween(160, 200),
        'weight' => $faker->randomFloat(2,50,130),
        'token' => $faker->asciify('***********'),
        'emailFamily' => $faker->email(),
        'birthday' => $faker->date(),
        'telnr' => $faker->phoneNumber(),
        'physiotherapist_id' => random_int(\DB::table('physiotherapists')->min('id'), \DB::table('physiotherapists')->max('id')),
        'max_bpm' => $faker->numberBetween(100,130),
        'goal' => $faker->numberBetween(1000, 10000),
    ];
});
